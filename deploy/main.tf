terraform {
  backend "s3" {
    bucket         = "dod-app-api-devops-tfstate"
    key            = "dod-app-tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "dod-app-api-devops-tfstate-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManageBy    = "Terraform"
  }
}


data "aws_region" "current" {}
