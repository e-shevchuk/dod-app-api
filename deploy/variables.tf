variable "prefix" {
  default = "dod"
}

variable "project" {
  default = "dod_app_api_devops"
}

variable "contact" {
  default = "evgeniy.shevchuk@gmail.com"
}

variable "db_username" {
  description = "User name for the RDS Postgress instance"
}

variable "db_password" {
  description = "Password for the RDS Postgress instance"
}

variable "bastion_key_name" {
  default = "dod-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "274184025015.dkr.ecr.us-east-1.amazonaws.com/dod-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "274184025015.dkr.ecr.us-east-1.amazonaws.com/dod-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret eky for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "aitimeassistant.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
